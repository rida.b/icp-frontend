import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Login from '../components/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    // path: '/',
    // name: 'Home',
    // component: Home
    path: '/',
    name: 'Login',
    component: Login

  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/Dashboard.vue')

  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue')

  },
  {
    path: '/books',
    name: 'Books',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Books.vue')

  },
  {
    path: '/detail',
    name: 'Detail',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Detail.vue')

  },
  {
    path: '/edit',
    name: 'Edit',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Edit.vue')

  },
  {
    path: '/create',
    name: 'Create',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Create.vue')

  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
