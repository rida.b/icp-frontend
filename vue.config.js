const path = require('path');

module.exports = {
	devServer: {
	  proxy: {
			"/api": {
				target: "https://test.incenplus.com",
				secure: false,
				logLevel: "debug",
				pathRewrite: {
					"^/api": "",
				},
				changeOrigin: true,
			},
        }
    }
}